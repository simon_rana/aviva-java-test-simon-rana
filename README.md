# README #

### Assumptions ###

Please be aware that I made the following assumptions when developing this test solution:

1 ) The test did not stipulate whether the system should remember past request bookings, or whether each batch of booking requests should be treated as standalone. I considered the former to be more likely and have implemented accordingly.

2 ) Styling of the UI has been kept simple and clean as the test appears to be focused on business logic rather than visual design. If you are looking for more focus on aesthetic flair please let me know and I can put more time into the styling.

3 ) The test made no mention of how the system should cope with bad data, or even whether it should. I therefore made my own assumptions on the subject and implemented strict validation that returns feedback aimed at a non-technical user.

### Using the solution ###

I've tried to keep the solution quick, simple and to the point. Please run BookingInterface.main to access the UI, which consists of a text area and two action buttons. To process a booking request batch please type or paste it into the text area and press "Submit". 

If a validation error is encountered it will be reported via a dialog box. If the batch passes validation the approved booking requests will be reported back in the text area, which will now be read-only.

To enter another batch that will processed *in addition* to the approved booking requests press "Submit Again". Please note that if the office hours have been changed in the new batch the previously approved booking requests will be reprocessed accordingly.

To wipe the past history of approved booking requests and enter a brand new batch simply press "Clear History".
 
### Tests ###
Tests have been implemented for BookingRequest and BookingRequests. I have not implemented tests for BookingInterface because (full disclosure) I did not have the time to start playing around with swing tests. If this is a problem please let me know and I will commit some further time to adding these tests.