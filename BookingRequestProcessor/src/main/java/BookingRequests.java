package main.java;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class BookingRequests
{
    private LocalTime startTime;
    private LocalTime endTime;
    private List<BookingRequest> bookingRequests;

    public BookingRequests()
    {
        startTime = endTime = null;
        bookingRequests = new ArrayList<>();
    }

    public LocalTime getStartTime() { return startTime; }

    public LocalTime getEndTime() { return endTime; }

    public void setOfficeHours( String line ) throws Exception
    {
        // Validate that office hours have been correctly formatted and valid before storing values
        LocalTime newStartTime, newEndTime;

        try
        {
            DateTimeFormatter dtFormat = DateTimeFormatter.ofPattern( "HHmm" );
            newStartTime = LocalTime.parse( line.substring( 0, 4 ), dtFormat );
            newEndTime = LocalTime.parse( line.substring( 5, 9 ), dtFormat );
        }
        catch ( Exception ex )
        {

            throw new Exception( String.format( "Office hours does not conform to expected format of 'HHmm HHmm' in line '%s'",
                                  line ) );
        }

        if ( !newEndTime.isAfter( newStartTime ) )
            throw new Exception( "Office hours are invalid - start time must be earlier than end time" );

        boolean hoursChange = ( startTime != null && ( !startTime.equals( newStartTime ) || !endTime.equals( newEndTime ) ) );

        startTime = newStartTime;
        endTime = newEndTime;

        if ( hoursChange )
            removeOutOfHoursRequests();
    }

    public void addBookingRequest( BookingRequest bookingRequest )
    {
        if ( isInOfficeHours( bookingRequest ) )
            bookingRequests.add( bookingRequest );
    }

    public void removeOutOfHoursRequests()
    {
        ListIterator<BookingRequest> iterator = bookingRequests.listIterator();

        while( iterator.hasNext() )
            if( !isInOfficeHours(iterator.next() ) )
                iterator.remove();
    }

    public boolean isInOfficeHours( BookingRequest bookingRequest )
    {
        if ( !bookingRequest.getStartDt().toLocalTime().isBefore( startTime ) &&
             !bookingRequest.getEndDt().toLocalTime().isAfter( endTime ) )
            return true;

        return false;
    }

    public List<BookingRequest> getApprovedBookingRequests()
    {
        List<BookingRequest> approvedBookingRequests = new ArrayList<>();

        // Sort by submission date
        bookingRequests.sort( ( BookingRequest o1, BookingRequest o2 ) ->
                              o1.getSubmissionDt().compareTo( o2.getSubmissionDt() ) );

        // Only take booking request as approved if there is overlap with an existing
        // approved booking request
        for ( BookingRequest unapprovedBr : bookingRequests )
        {
            boolean overlap = false;

            for ( BookingRequest approvedBr : approvedBookingRequests )
            {
                if ( approvedBr.overlaps( unapprovedBr ) )
                {
                    overlap = true;
                    break;
                }
            }

            if ( !overlap )
                approvedBookingRequests.add( unapprovedBr );
        }

        // Sort by approved results by start date
        approvedBookingRequests.sort( ( BookingRequest o1, BookingRequest o2 ) ->
                                      o1.getStartDt().compareTo( o2.getStartDt() ) );

        return approvedBookingRequests;
    }
}
