package main.java;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class BookingInterface extends JFrame
{
    private JLabel label;
    private JTextArea textArea;
    private JScrollPane scrollPane;
    private JButton buttonClear;
    private JButton buttonToggle;
    private BookingRequests bookingRequests;

    private String clearCommand = "Clear History";
    private String editCommand = "Submit";
    private String noneditCommand = "Submit Again";

    public BookingInterface()
    {
        setLayout( new FlowLayout() );
        setTitle( "Booking Requests" );

        label = new JLabel();
        textArea = new JTextArea( 20, 20 );
        textArea.setLineWrap( true );
        textArea.setWrapStyleWord( true );
        scrollPane = new JScrollPane( textArea );

        buttonClear = new JButton( clearCommand );
        buttonToggle = new JButton();

        add( label );
        add( scrollPane );
        add( buttonClear );
        add( buttonToggle );

        event clear = new event();
        buttonClear.addActionListener( clear );

        event submit = new event();
        buttonToggle.addActionListener( submit );

        clearHistory();
    }


    public void clearHistory()
    {
        bookingRequests = new BookingRequests();
        textArea.setText( "" );
        interfaceMode( true );
    }

    public void interfaceMode( boolean allowEdit )
    {
        // use this method to toggle interface between edit modes (used
        // to enter booking requests) and non-edit mode (used to show
        // confirmed booking requests)
        textArea.setEditable( allowEdit );
        textArea.setText( "" );

        if ( allowEdit )
        {
            label.setText( "Please enter booking requests below:" );
            buttonToggle.setText( editCommand );
        }
        else
        {
            label.setText( "The following bookings are confirmed" );
            buttonToggle.setText( noneditCommand );
        }
    }

    public class event implements ActionListener
    {
        public void actionPerformed( ActionEvent e )
        {
            if ( e.getActionCommand().equals( clearCommand ) )
            {
                clearHistory();
            }
            else if ( e.getActionCommand().equals( noneditCommand ) )
            {
                interfaceMode( true );
            }
            else
            {
                try
                {
                    // Will throw a user-friendly error in the event of validation issue
                    validateInput();

                    interfaceMode( false );
                    output( bookingRequests.getApprovedBookingRequests() );
                }
                catch( Exception ex )
                {
                    JOptionPane.showMessageDialog( null, ex.getMessage() );
                }
            }
        }
    }

    private void validateInput() throws Exception
    {
        // Constructors handle all validation, other than checking that the right
        // number of lines have been supplied

        String[] lines = textArea.getText().split( "\\r?\\n" );

        // Validate that there are enough lines for at least one booking request
        if ( lines.length < 3 )
            throw new Exception( "Please enter office hours and at least one booking request" );

        bookingRequests.setOfficeHours( lines[ 0 ] );

        for ( int i = 1; i < lines.length; i+=2 )
        {
            if ( i + 1 == lines.length )
                throw new Exception( "Missing second line for final booking request" );

            bookingRequests.addBookingRequest( new BookingRequest( lines[i], lines[i+1] ) );
        }
    }

    private void output( List<BookingRequest> approvedBookingRequests )
    {
        textArea.setText( "" );

        DateTimeFormatter dtFormatterDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter dtFormatterHour = DateTimeFormatter.ofPattern("HH:mm");

        BookingRequest previousBr = null;

        for( BookingRequest approvedBr : approvedBookingRequests )
        {
            if ( previousBr == null || !approvedBr.isSameDate( previousBr ) )
                textArea.append( approvedBr.getStartDt().format( dtFormatterDate ) + "\n" );

            textArea.append( String.format( "%s %s %s\n",
                                            approvedBr.getStartDt().format( dtFormatterHour ),
                                            approvedBr.getEndDt().format( dtFormatterHour ),
                                            approvedBr.getEmployeeId() ) );
            previousBr = approvedBr;
        }
    }

    public static void main( String args[] )
    {
        BookingInterface inputUI = new BookingInterface();
        inputUI.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        inputUI.setSize( 300, 430 );
        inputUI.setVisible( true );
    }
}
