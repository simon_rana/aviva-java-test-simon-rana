package main.java;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class BookingRequest
{
    private LocalDateTime submissionDt;
    private LocalDateTime startDt;
    private LocalDateTime endDt;
    private String employeeId;

    public BookingRequest( String submissionLine, String appointmentLine ) throws Exception
    {
        // Constructor will throw a user-friendly error message in the event of
        // validation failure

        String submissionFormat = "yyyy-MM-dd HH:mm:ss";
        String appointmentFormat = "yyyy-MM-dd HH:mm";

        submissionLine = submissionLine.trim();
        appointmentLine = appointmentLine.trim();

        submissionDt = getLocalDateTime( submissionLine, submissionFormat );

        if ( submissionLine.length() == submissionFormat.length() )
            throw new Exception( String.format( "Employee ID not found in line '%s'", submissionLine ) );

        employeeId = submissionLine.substring( submissionFormat.length() + 1, submissionLine.length() );
        startDt = getLocalDateTime( appointmentLine, appointmentFormat );

        try
        {
            Double hours = Double.valueOf( appointmentLine.substring( appointmentFormat.length() + 1,
                                                                      appointmentLine.length() ) );

            Long minutes = (long) ( hours * 60 );

            // Minutes cannot be zero or large enough to make meeting span
            // more than one day
            if ( minutes == 0 )
                throw new Exception();

            endDt = startDt.plusMinutes( minutes );

            if ( startDt.getYear() != endDt.getYear() ||
                 startDt.getMonth() != endDt.getMonth() ||
                 startDt.getDayOfMonth() != endDt.getDayOfMonth() )
                throw new Exception( );
        }
        catch ( Exception ex )
        {
            throw new Exception( String.format( "Meeting duration is not valid in line '%s'", appointmentLine ) );
        }
    }

    private LocalDateTime getLocalDateTime( String line, String format ) throws Exception
    {
        try
        {
            return LocalDateTime.parse( line.substring( 0, format.length() ), DateTimeFormatter.ofPattern( format ) );
        }
        catch( Exception ex )
        {
            throw new Exception( String.format( "Date time does not conform to expected format '%s' in line '%s'",
                                                format, line ) );
        }
    }

    public LocalDateTime getSubmissionDt() { return submissionDt; }

    public LocalDateTime getStartDt() { return startDt; }

    public LocalDateTime getEndDt() { return endDt; }

    public String getEmployeeId() { return employeeId; }

    public boolean overlaps( BookingRequest o )
    {
        return ( this.startDt.isBefore( o.getEndDt() ) & o.getStartDt().isBefore( this.endDt ) );
    }

    public boolean isSameDate( BookingRequest o )
    {
        if ( startDt.getYear() != o.getStartDt().getYear() ||
             startDt.getMonth() != o.getStartDt().getMonth() ||
             startDt.getDayOfMonth() != o.getStartDt().getDayOfMonth() )
            return false;

        return true;
    }
}

