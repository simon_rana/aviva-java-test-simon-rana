import junit.framework.Assert;
import main.java.BookingRequest;
import main.java.BookingRequests;
import org.testng.annotations.Test;

import java.time.LocalTime;
import java.util.List;

public class BookingRequestsTest
{
    @Test
    public void testSetOfficeHours_validData() throws Exception
    {
        BookingRequests bookingRequests = new BookingRequests();
        bookingRequests.setOfficeHours( "0900 1730" );

        Assert.assertEquals( bookingRequests.getStartTime(), LocalTime.of( 9, 0 ) );
        Assert.assertEquals( bookingRequests.getEndTime(), LocalTime.of( 17, 30 ) );
    }

    @Test(expectedExceptions=Exception.class)
    public void testSetOfficeHours_invalidFormat() throws Exception
    {
        BookingRequests bookingRequests = new BookingRequests();
        bookingRequests.setOfficeHours( "0900 17aa" );
    }

    @Test(expectedExceptions=Exception.class)
    public void testSetOfficeHours_invalidHours() throws Exception
    {
        BookingRequests bookingRequests = new BookingRequests();
        bookingRequests.setOfficeHours( "0900 0900" );
    }

    @Test
    public void testSetOfficeHours_hoursChanged() throws Exception
    {
        BookingRequests bookingRequests = new BookingRequests();
        bookingRequests.setOfficeHours( "0900 1730" );
        bookingRequests.setOfficeHours( "0900 1700" );

        Assert.assertEquals( bookingRequests.getStartTime(), LocalTime.of( 9, 0 ) );
        Assert.assertEquals( bookingRequests.getEndTime(), LocalTime.of( 17, 0 ) );
    }

    @Test
    public void testRemoveOutOfHoursRequests() throws Exception
    {
        BookingRequests bookingRequests = new BookingRequests();
        bookingRequests.setOfficeHours( "0900 1730" );

        String submissionLine = "2016-01-01 09:00:00 EMP001";
        BookingRequest br1 = new BookingRequest( submissionLine, "2016-01-01 09:00 1" );
        BookingRequest br2 = new BookingRequest( submissionLine, "2016-01-01 16:30 1" );

        bookingRequests.addBookingRequest( br1 );
        bookingRequests.addBookingRequest( br2 );

        List<BookingRequest> approvedBr = bookingRequests.getApprovedBookingRequests();

        Assert.assertEquals( approvedBr.get( 0 ), br1 );
        Assert.assertEquals( approvedBr.get( 1 ), br2 );

        bookingRequests.setOfficeHours( "0900 1700" );
        bookingRequests.removeOutOfHoursRequests();

        approvedBr = bookingRequests.getApprovedBookingRequests();

        Assert.assertEquals( approvedBr.size(), 1 );
        Assert.assertEquals( approvedBr.get( 0 ), br1 );
    }

    @Test
    public void testIsInOfficeHours() throws Exception
    {
        BookingRequests bookingRequests = new BookingRequests();
        bookingRequests.setOfficeHours( "0900 1730" );

        String submissionLine = "2016-01-01 09:00:00 EMP001";
        BookingRequest br1 = new BookingRequest( submissionLine, "2016-01-01 08:00 1" );
        BookingRequest br2 = new BookingRequest( submissionLine, "2016-01-01 09:00 1" );
        BookingRequest br3 = new BookingRequest( submissionLine, "2016-01-02 16:00 1.5" );
        BookingRequest br4 = new BookingRequest( submissionLine, "2016-01-02 16:00 2" );

        Assert.assertEquals( bookingRequests.isInOfficeHours( br1 ), false );
        Assert.assertEquals( bookingRequests.isInOfficeHours( br2 ), true );
        Assert.assertEquals( bookingRequests.isInOfficeHours( br3 ), true );
        Assert.assertEquals( bookingRequests.isInOfficeHours( br4 ), false );
    }

    @Test
    public void testGetApprovedBookingRequests() throws Exception
    {
        BookingRequests bookingRequests = new BookingRequests();
        bookingRequests.setOfficeHours( "0900 1730" );

        BookingRequest br1 = new BookingRequest( "2016-07-18 10:17:06 EMP001", "2016-07-21 09:00 2" );
        BookingRequest br2 = new BookingRequest( "2016-07-18 12:34:56 EMP002", "2016-07-21 09:00 2" );
        BookingRequest br3 = new BookingRequest( "2016-07-18 09:28:23 EMP003", "2016-07-22 14:00 2" );
        BookingRequest br4 = new BookingRequest( "2016-07-18 11:23:45 EMP004", "2016-07-22 16:00 1" );
        BookingRequest br5 = new BookingRequest( "2016-07-15 17:29:12 EMP005", "2016-07-21 16:00 3" );

        bookingRequests.addBookingRequest( br1 );
        bookingRequests.addBookingRequest( br2 );
        bookingRequests.addBookingRequest( br3 );
        bookingRequests.addBookingRequest( br4 );
        bookingRequests.addBookingRequest( br5 );

        bookingRequests.removeOutOfHoursRequests();
        List<BookingRequest> approvedBr = bookingRequests.getApprovedBookingRequests();

        Assert.assertEquals( approvedBr.size(), 3 );
        Assert.assertEquals( approvedBr.get( 0 ), br1 );
        Assert.assertEquals( approvedBr.get( 1 ), br3 );
        Assert.assertEquals( approvedBr.get( 2 ), br4 );
    }
}
