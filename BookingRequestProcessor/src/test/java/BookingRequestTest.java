import junit.framework.Assert;
import main.java.BookingRequest;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

public class BookingRequestTest
{
    private String submissionLine = "2016-01-01 09:00:00 EMP001";

    @Test
    public void testConstructor_validData() throws Exception
    {
        BookingRequest br = new BookingRequest( submissionLine, "2016-07-21 09:00 2" );

        Assert.assertEquals( br.getSubmissionDt(), LocalDateTime.of( 2016, 1, 1, 9, 0, 0 ) );
        Assert.assertEquals( br.getEmployeeId(), "EMP001" );
        Assert.assertEquals( br.getStartDt(), LocalDateTime.of( 2016, 7, 21, 9, 0, 0 ) );
        Assert.assertEquals( br.getEndDt(), LocalDateTime.of( 2016, 7, 21, 11, 0, 0 ) );
    }

    @Test(expectedExceptions=Exception.class)
    public void testConstructor_invalidSubmissionDate() throws Exception
    {
        new BookingRequest( "2016-07-18 10:17 EMP001", "2016-07-21 09:00 2" );
    }

    @Test(expectedExceptions=Exception.class)
    public void testConstructor_invalidEmployeeId() throws Exception
    {
        new BookingRequest( "2016-07-18 10:17:06   ", "2016-07-21 09:00 2" );
    }

    @Test(expectedExceptions=Exception.class)
    public void testConstructor_invalidAppointmentDate() throws Exception
    {
        new BookingRequest( submissionLine, "2016-07-21 09 2" );
    }

    @Test(expectedExceptions=Exception.class)
    public void testConstructor_nonNumericHours() throws Exception
    {
        new BookingRequest( submissionLine, "2016-07-21 09:00 a" );
    }

    @Test(expectedExceptions=Exception.class)
    public void testConstructor_zeroHours() throws Exception
    {
        new BookingRequest( submissionLine, "2016-07-21 09:00 0" );
    }

    @Test(expectedExceptions=Exception.class)
    public void testConstructor_excessiveHours() throws Exception
    {
        new BookingRequest( submissionLine, "2016-07-21 09:00 24" );
    }

    @Test
    public void testOverlaps() throws Exception
    {
        BookingRequest br1 = new BookingRequest( submissionLine, "2016-01-01 09:00 1" );
        BookingRequest br2 = new BookingRequest( submissionLine, "2016-01-01 10:00 2" );
        BookingRequest br3 = new BookingRequest( submissionLine, "2016-01-01 11:00 1" );
        BookingRequest br4 = new BookingRequest( submissionLine, "2016-01-01 12:00 1" );
        BookingRequest br5 = new BookingRequest( submissionLine, "2016-01-01 09:00 8" );
        BookingRequest br6 = new BookingRequest( submissionLine, "2016-01-02 09:00 1" );

        Assert.assertEquals( br1.overlaps( br1 ), true );
        Assert.assertEquals( br1.overlaps( br2 ), false );
        Assert.assertEquals( br2.overlaps( br3 ), true );
        Assert.assertEquals( br2.overlaps( br4 ), false );
        Assert.assertEquals( br2.overlaps( br5 ), true );
        Assert.assertEquals( br1.overlaps( br6 ), false );
    }

    @Test
    public void testIsSameDate() throws Exception
    {
        BookingRequest br1 = new BookingRequest( submissionLine, "2016-01-01 09:00 1" );
        BookingRequest br2 = new BookingRequest( submissionLine, "2016-01-01 10:00 2" );
        BookingRequest br3 = new BookingRequest( submissionLine, "2016-01-02 09:00 1" );

        Assert.assertEquals( br1.isSameDate( br2 ), true );
        Assert.assertEquals( br1.isSameDate( br3 ), false );
    }
}
